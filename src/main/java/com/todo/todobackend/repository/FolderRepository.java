package com.todo.todobackend.repository;

import com.todo.todobackend.model.Folder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FolderRepository extends JpaRepository<Folder, Long> {
    List<Folder> findFolderByUserId(Long id);

    Folder findFolderById(Long id);
}
