package com.todo.todobackend.repository;

import com.todo.todobackend.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findTasksByFolderId(Long id);
    Task findTaskById(Long id);

//    @Query("UPDATE tasks t SET t.done = :done WHERE t.id = :id")
//    void restore(@Param("id") Long id, @Param("done") boolean done);
}
