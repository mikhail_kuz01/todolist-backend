package com.todo.todobackend.controller;

import com.todo.todobackend.model.Folder;
import com.todo.todobackend.repository.FolderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/folder")
public class FolderController {

    @Autowired
    private FolderRepository folderRepository;

    @GetMapping("/user/{id}")
    public List<Folder> getFolderByUser(@PathVariable Long id) {
        return folderRepository.findFolderByUserId(id);
    }

    @PostMapping("/create")
    public boolean createFolder(@RequestBody Folder folder) {
        try {
            folderRepository.save(folder);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @DeleteMapping("/delete/{id}")
    public boolean deleteFolder(@PathVariable Long id) {
        try {
            Folder folderForDelete = folderRepository.findFolderById(id);
            folderRepository.delete(folderForDelete);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
