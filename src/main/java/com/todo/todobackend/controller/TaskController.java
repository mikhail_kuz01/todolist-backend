package com.todo.todobackend.controller;

import com.todo.todobackend.model.Task;
import com.todo.todobackend.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/task")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @GetMapping("/folder/{id}")
    public List<Task> getTaskByFolder(@PathVariable Long id) {
        return taskRepository.findTasksByFolderId(id);
    }

    @PostMapping("/create")
    public boolean createTask(@RequestBody Task task) {
        try {
            taskRepository.save(task);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @DeleteMapping("/delete/{id}")
    public boolean deleteTask(@PathVariable Long id) {
        try {
            Task taskForDelete = taskRepository.findTaskById(id);
            taskRepository.delete(taskForDelete);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
