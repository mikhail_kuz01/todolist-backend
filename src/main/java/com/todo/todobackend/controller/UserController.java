package com.todo.todobackend.controller;

import com.todo.todobackend.model.Authority;
import com.todo.todobackend.model.User;
import com.todo.todobackend.repository.UserDetailsRepository;
import com.todo.todobackend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/find/{username}")
    public ResponseEntity<?> getUserByUserName(@PathVariable String username) {
        User user = (User) userDetailsService.loadUserByUsername(username);
        return ResponseEntity.ok(user.getId());
    }

    @PostMapping("/create")
    public boolean createUser(@RequestBody User user) {
        try {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setEnabled(true);

            if (user.getCreatedAt() == null) {
                user.setCreatedAt(new Date());
            }

            userRepository.save(user);

            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
