package com.todo.todobackend.config;

import com.todo.todobackend.model.Authority;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JWTAuthenticationFilter extends OncePerRequestFilter {

    private UserDetailsService userDetailsService;
    private JWTTokenHelper jwtTokenHelper;

    public JWTAuthenticationFilter(UserDetailsService userDetailsService, JWTTokenHelper jwtTokenHelper) {
        this.userDetailsService = userDetailsService;
        this.jwtTokenHelper = jwtTokenHelper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authToken = jwtTokenHelper.getToken(request);

        if (authToken != null) {
            String userName = jwtTokenHelper.getUsernameFromToken(authToken);

            if(userName != null) {
                UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

                if(jwtTokenHelper.validateToken(authToken, userDetails)) {
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, initAuthority());
                    authentication.setDetails(new WebAuthenticationDetails(request));

                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }

        filterChain.doFilter(request, response);
    }

    private List<Authority> initAuthority() {
        List<Authority> authorityList = new ArrayList<>();

        authorityList.add(createAuthority(1L, "USER","User role"));
        authorityList.add(createAuthority(2L, "ADMIN","Admin role"));

        return authorityList;
    }

    private Authority createAuthority(Long id, String roleCode, String roleDescription) {
        Authority authority = new Authority();
        authority.setId(id);
        authority.setRoleCode(roleCode);
        authority.setRoleDescription(roleDescription);

        return authority;
    }
}
